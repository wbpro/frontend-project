import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useMessageStore } from "./message";
import { useLoadingStore } from "./loading";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const authName = ref("");
  const messageStore = useMessageStore();
  const loading = useLoadingStore();

  const login = async (username: string, password: string): Promise<void> => {
    loading.isLoading = true;
    try {
      const res = await auth.login(username, password);
      localStorage.setItem("token", res.data.access_token);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      router.push("/");
    } catch (e) {
      console.log(e);
      messageStore.showError("Username or Password no correct");
    }
    loading.isLoading = false;
  };
  const logout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    router.replace("/login");
  };

  function isLogin() {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  }
  return { authName, isLogin, login, logout };
});
